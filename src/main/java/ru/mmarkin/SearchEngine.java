package ru.mmarkin;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import static javax.swing.text.Highlighter.HighlightPainter;

class SearchEngine implements DocumentListener {

    private final Color reg_color = Color.GRAY;
    private final HighlightPainter reg_painter = new DefaultHighlightPainter(reg_color);
    private final Color spec_color = Color.YELLOW;
    private final HighlightPainter spec_painter = new DefaultHighlightPainter(spec_color);
    private final Highlighter colorer = new DefaultHighlighter();
    private final RemoveHighlightAction clearAllAction;
    private final HighlightNextAction highlightNextAction;
    private final JLabel statusLabel;
    private final JTextField regexField;
    private final JTextArea logAreaField;

    private final JCheckBox matchCase;
    private final List<SearchState> state = new ArrayList<SearchState>();

    private int curPos = -1;

    public SearchEngine(JTextField regexField, JTextArea logAreaField, JLabel statusLabel, JCheckBox matchCase) {
        this.regexField = regexField;
        this.logAreaField = logAreaField;
        this.statusLabel = statusLabel;
        this.logAreaField.setHighlighter(colorer);
        this.clearAllAction = new RemoveHighlightAction();
        this.highlightNextAction = new HighlightNextAction();
        this.matchCase = matchCase;
    }

    public AbstractAction getHighlightNextAction() {
        return highlightNextAction;
    }

    public AbstractAction getClearAllAction() {
        return clearAllAction;
    }

    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        search();
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        search();
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {

    }

    public void search() {
        int entriesCount = cleanSelected();
        String searchText = regexField.getText();
        if (searchText.isEmpty()) return;

        String text = logAreaField.getText();
        Pattern regex;
        try {
            if (matchCase.isSelected()) {
                regex = Pattern.compile(searchText);
            } else {
                regex = Pattern.compile(searchText, Pattern.CASE_INSENSITIVE);
            }
        } catch (PatternSyntaxException pe) {
            updateStatus("error in regex");
            return;
        }
        Matcher matcher = regex.matcher(text);
        while (matcher.find()) {

            try {
                Object tag = colorer.addHighlight(matcher.start(), matcher.end(), reg_painter);
                state.add(new SearchState(matcher.start(), matcher.end(), tag));
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
            entriesCount++;
        }
        updateStatus("Found " + entriesCount + " entries");
    }

    private int cleanSelected() {
        state.clear();
        colorer.removeAllHighlights();
        int entriesCount = 0;
        updateStatus("");
        curPos = -1; //TODO need to verify behavior
        return entriesCount;
    }

    private void updateStatus(String text) {
        statusLabel.setText(text);
    }

    private class RemoveHighlightAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            cleanSelected();
        }
    }

    private class HighlightNextAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (state.size() == 0) return;
            SearchState prevState;
            SearchState curState;
            try {
                if (curPos == -1) {
                    curState = state.get(++curPos);
                    colorer.removeHighlight(curState.getTag());
                    curState.setTag(colorer.addHighlight(curState.getStart(), curState.getEnd(), spec_painter));
                    logAreaField.setCaretPosition(curState.getStart());
                    return;
                } else if (curPos == state.size() - 1) {
                    prevState = state.get(curPos);
                    curPos = 0;
                } else {
                    prevState = state.get(curPos);
                    curPos++;
                }
                curState = state.get(curPos);
                colorer.removeHighlight(prevState.getTag());
                colorer.removeHighlight(curState.getTag());
                curState.setTag(colorer.addHighlight(curState.getStart(), curState.getEnd(), spec_painter));
                prevState.setTag(colorer.addHighlight(prevState.getStart(), prevState.getEnd(), reg_painter));
                logAreaField.setCaretPosition(curState.getStart());
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    }
}

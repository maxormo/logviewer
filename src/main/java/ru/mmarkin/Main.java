package ru.mmarkin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * program entry point
 */
public class Main extends JFrame {

    private final JTextField regexField;
    private final JLabel regexLabel;
    private final JTextArea logAreaField;
    private final JScrollPane scrollPane;
    private final JLabel statusLabel;
    private final JCheckBox caseCheckBox;

    private Main() {
        regexField = new JTextField(50);
        regexLabel = new JLabel("Regex for search: ");
        logAreaField = new JTextArea(20, 50);
        caseCheckBox = new JCheckBox("Match case", false);
        statusLabel = new JLabel();
        final SearchEngine searchEngine = new SearchEngine(regexField, logAreaField, statusLabel, caseCheckBox);

        ActionMap am = regexField.getActionMap();
        InputMap im = regexField.getInputMap();
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), FindActions.CANCEL_SEARCH);
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), FindActions.FIND_NEXT_SEARCH);

        am.put(FindActions.CANCEL_SEARCH, searchEngine.getClearAllAction());
        am.put(FindActions.FIND_NEXT_SEARCH, searchEngine.getHighlightNextAction());

        regexField.getDocument().addDocumentListener(searchEngine);
        caseCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchEngine.search();
            }
        });
        scrollPane = new JScrollPane(logAreaField);

        logAreaField.setEditable(false);
        initComponents();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    private void initComponents() {
        initMenu();

        setTitle("Log viewer");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel(new BorderLayout(0, 5));
        Box hBox = Box.createHorizontalBox();

        panel.add(hBox, BorderLayout.NORTH);
        panel.add(scrollPane, BorderLayout.CENTER);
        panel.add(statusLabel, BorderLayout.SOUTH);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        hBox.add(regexLabel);
        hBox.add(regexField);
        hBox.add(caseCheckBox);
        getContentPane().add(panel);
        pack();
    }

    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);
        JMenuItem openFileMenu = new JMenuItem("Open");
        JMenuItem exiMenuItem = new JMenuItem("Exit");

        openFileMenu.setMnemonic(KeyEvent.VK_O);
        exiMenuItem.setMnemonic(KeyEvent.VK_E);

        fileMenu.add(openFileMenu);
        fileMenu.add(exiMenuItem);

        openFileMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File selectedFile = openFileChooser(getContentPane());
                loadFile(selectedFile);
            }
        });

        exiMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        getRootPane().setJMenuBar(menuBar);
    }

    private void loadFile(File file) {
        if (file.exists()) {
            try {
                InputStreamReader is = new InputStreamReader(new FileInputStream(file));
                logAreaField.read(is, null);
            } catch (IOException ignored) {

            }
        }
    }

    private File openFileChooser(Container contentPane) {
        JFileChooser fileChooser = new JFileChooser();
        int res = fileChooser.showOpenDialog(contentPane);
        if (res == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        }
        return new File("");
    }

    private enum FindActions {
        CANCEL_SEARCH, FIND_NEXT_SEARCH
    }
}

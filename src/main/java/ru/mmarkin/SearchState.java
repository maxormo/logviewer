package ru.mmarkin;

class SearchState {
    private final int start, end;
    private Object tag;

    public SearchState(int start, int end, Object tag) {
        this.start = start;
        this.end = end;
        this.tag = tag;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }
}